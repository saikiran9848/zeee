const AWS = require("aws-sdk");
AWS.config.update({
	region: process.env.AWS_REGION,
});
if(process.env.AWS_END_POINT){
	AWS.config.update({
		endpoint: process.env.AWS_END_POINT,
	});
}

module.exports = {
	dynamo: new AWS.DynamoDB.DocumentClient(),
	dynamoDB:new AWS.DynamoDB(),
	sqs: new AWS.SQS({apiVersion: 'latest'}),
	SecretsManager:new AWS.SecretsManager(),
};