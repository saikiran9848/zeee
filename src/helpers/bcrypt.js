const bcrypt = require('bcrypt');
const logger = require('../helpers/logger')

module.exports ={
    createPasswordHash : async (password) => {
        try {
            const saltRounds = 10;
            return hash = bcrypt.hashSync(password, saltRounds);
        } 
        catch (error) {
            logger.error(JSON.stringify(error));
        }  
    },
    verifyPasswordHash : async ( password,hash)=>{
        return (bcrypt.compareSync(password, hash)).toString();
    }    
}

