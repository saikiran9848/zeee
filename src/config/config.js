module.exports = {
    wistonLevels: {
        error: 0,
        warn: 1,
        info: 2,
        http: 3,
        debug: 4,
    },

    wistonColor: {
        error: "red",
        warn: "yellow",
        info: "green",
        http: "magenta",
        debug: "white",
    },
    favorites: {
        table: "userapi-favorites",
        index: "user_index",
        redis_prefix: "favorites_user_",
    },
    reminders: {
        table: "userapi-reminders",
        index: "user_index",
        redis_prefix: "reminders_user_",
    },
    watchlist: {
        table: "userapi-watchlist",
        index: "user_index",
        redis_prefix: "watchlist_user_",
    },
    settings: {
        table: "userapi-settings",
        index: "user_index",
        redis_prefix: "watchlist_user_",
    },
    users: {
        table: "userapi-users",
        redis_prefix: "user_",
        email_index: "email_index"
    },
    onetimepassword: {
        table: "onetimepassword",
        redis_prefix: "verify_code_",
    },
    helthcheck:{
        table: "userapi.helthcheck",
    },
    userProfileUpdateHistory:{
        table: "userapi-userProfileUpdateHistory",
        index: "user_index",
    },
    TokenServiceOptions: {
        // ClientTokenLifeTime: 31536000,
        // ClientTokenSecret: "17360741-3942-436a-b3d8-3c4ecb6417fa",
        // SlidingRefreshTokenLifetime: 15552000,
        AccessTokenLifeTime: '10368000s',
        // RefreshClientTokenLifeTime: 0,
        // RefreshClientTokenSecret: "5c1462b7-920a-4946-865a-1bbfe355766d",
        // RsaKey: "eyJEIjoiUk9xQ3hJcTF5ZkNJOFBEN3JoTndUV3VhMDJ5WkVRNEd0QWhCNmhJNVBGeG9HREs0SDlmUEtpNUZPMmxWZ2ZuU0JZK2Vqei9qZjMwdTZsdmdaSHY5cGZRdk5yMk1xRitBQWFPSFU3YWJUOXNDR29uL053d2VleXJ0RjBqNTVjVThHVXRmWVgzNVBYcUkwc09mZjJHd0lnc0c0ZDc0UFBmWjdDc1UyTEdNVkNmcE1lM2JqOW1YVDRLY1hRWkNGeVlwL0NiTVFTNExZRDJCOG9veGV5M2xHMHcvcVdRbENYSW1GcXFCLzJQcFlGdWQwK2JjcURpbkFrMWoxcWhOMnZhaEExZEpScE5QSGRUbkFtSkJKYnZDM2VJUjV5ckVtWU1oZVZzQ1hLUHExdlFWUVN0OEgzYm02R25iM256Zzl4TmpqMzJUdTBoYkV3Q2s0NjdsRFFHMDFRPT0iLCJEUCI6IkJXYWNhOXVaRmRjbm1oWjZPcEpjZG5aYVFTeXo3WDNJeUJUMGE2NzVzMU1aeHBEQWhlU0tWNVUyMW1IMlJLZ0FpK2hWais2dkRvSGtDdWVlMFpGR3orYW9ieUpDUnNZbG5PU3lkekxGWVBiUEc0UVdWNFNBTzhDOTV2VTdKeHFzNi9Va3RXSWFsSzZqUnlBZDIxSHdZQjVXS1ZyK0R6SXJibVlsTlpLSVpTOD0iLCJEUSI6IlBnSktvR0VJWUErL2s2OWtVS2tTU0JYc3RYMElmdTV5TGUrRC9MSk1nM0lyV1Y4U1AzMkxlZ3l3MG81RlJIbW51NkdKUm43eC9RdExocjF3dkl5a2xwOVhEaDljdWw0bU1tcVlhcGJlRHA3QmVoSll4ZGRob3pzR2F4UFd2c2V0dXpHYXB4Vk5FTjRvUEJwV0VYRkdTcDZ4WkpOQjdTdnNlOHhyNGY3K0Vpcz0iLCJFeHBvbmVudCI6IkFRQUIiLCJJbnZlcnNlUSI6Ik5QU3ZObEdMQmRMWnZQTlkrTzNTWUNLak9UeUVCS3UwSm5kcHNtYzZYbUhrVU5NL2REOGVISDFJbDhRV2kzZ3RXR0JMOUJyTGlnNFE3Tll3akNldW5SVTJqZzAwVEYzMW40eW1zNHY3UktWb0l4VHd4dTRpRkVyYlpVL1Q3cXlLNWlVZjZnSUJscnh6YUVLMndXVDNQRHRjNGQ0SGJPNFIveW5ZSElpV3pNbz0iLCJNb2R1bHVzIjoidGNvR1pqV0syZjRIM2VTemUrVW01L0NCYUNLcTFiYThwT1NFM0paRHc2MW9wR1dFWm9DYVZOdGlYaTd1OEtGR3dCb1hOcHJhdnJ0ZXVBa1loRU85TmF2VVRPcUNuSG4xN3oyNFREbzZQZ2hkeUFKOXNwSGJYMlhHTFVGZXBodzdOaWw5aW9keEdaK1psNy81Yjd5N01yenJ3eG8rd0FEbE15WUZmLzBWa0NFbVdwa0dSOW5DQXpXczJ5Ui95bHYvc0I1M0xkYndNNXY5M1hzMDBEbmdzZk55UGJFUDY4ZUxBSDFRWWhYU3dMMWJzbDZtZm1ESWFsdlVLbWR4K0JGUEltT095VVlBMUREY0thOW85d0JyWSt5ODd6WCtGdkw4SERWK3ZqNlFpSUxud2lnc1NsQmk1SFhWMVRGRU5QMFpBeWc0N1pHY0t2YlU1elF4ZkhseHVRPT0iLCJQIjoieXhKVlFWeUZrbmZFUWhySEJUb3F0dFI3NUR3V0R6dUdFMWs1MEo0OUpMQm8wWVRKVDUxVjhGM0VUMjNQVkRLemR6NVgzb3lPV1NUeUtmVE1DTk9ObktMSTVGZStXajRKalh3RGduVktJYzcwY2RyM2tmZ0lReDRPNzkwUGVJN0R1bVJEN3NuTWFvUGk0bU81YXdwaUhxZDZBYU5pU1MwdXBHK1E3Q0pkRTNNPSIsIlEiOiI1U3VtZWJHUVRxYXRVSlZVYVFCUkYyQWRSTHBRd2FtM0tOMXNmZ2lYdVBRKzc1ZUljamV1T2dINTF0ZmcrUHVabEFGbUkwRzhXZDdWWHJPNW94UWdVNndQOVhlczN4eTQyZkZZUE5wMTJkN3hMNUMxTlRxbHZQNzRUcEQvQ3F5eDlJY21wL096enF5V2t6T09BZDREVVo5NmlpcFZxK3VqeHNtT1RpUm8weU09In0=",
        // Audience: "userapi",
        // JwksString: "{\"keys\":[{\"kty\":\"RSA\",\"use\":\"sig\",\"e\":\"AQAB\",\"n\":\"tcoGZjWK2f4H3eSze-Um5_CBaCKq1ba8pOSE3JZDw61opGWEZoCaVNtiXi7u8KFGwBoXNpravrteuAkYhEO9NavUTOqCnHn17z24TDo6PghdyAJ9spHbX2XGLUFephw7Nil9iodxGZ-Zl7_5b7y7Mrzrwxo-wADlMyYFf_0VkCEmWpkGR9nCAzWs2yR_ylv_sB53LdbwM5v93Xs00DngsfNyPbEP68eLAH1QYhXSwL1bsl6mfmDIalvUKmdx-BFPImOOyUYA1DDcKa9o9wBrY-y87zX-FvL8HDV-vj6QiILnwigsSlBi5HXV1TFENP0ZAyg47ZGcKvbU5zQxfHlxuQ\",\"alg\":\"RS256\"}]}",
        TOKEN_SECRET:"32ac051493426639673163a8756790d5099ac66975dc53b13def13a094124d32ada0cccf507a1c15ed45050fcd5f46cfa31cc4d0a4cca319529f6a4a4882a302"
      },
};

