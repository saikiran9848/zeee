const _ = require("lodash");

deviceValidator = async (req, res, next) => {
    try{
        const header = req.headers;

        if (_.isEmpty(header['device_id']) && (_.isEmpty(header['esk']))){
            return res.status(401).send({
                code: 101, 
	            message: "device_id and esk is missing"
            })
        }
        if (_.isEmpty(header['device_id'])){
            return res.status(401).send({
                code: 102, 
	            message: "device_id is missing"
            })
        }
        if ((_.isEmpty(header['esk']))){
            return res.status(401).send({
                code: 103, 
	            message: "esk is missing"
            })
        }

        if(header['test'] === "wrong_device_id"){
            return res.status(401).send({
                code: 104,
                message: "wrong device id"
            })
        }
        if(header['test'] === "wrong_esk_id"){
            return res.status(401).send({
                code: 105,
                message: "wrong esk id"
            })
        }
        next();
    }catch(error){
        return res.status(401).send({
            errorCode: 401,
            message: error
        })
    }
}

module.exports = deviceValidator;