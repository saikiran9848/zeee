const response = require('../helpers/responseHelper')
const settingsServices =  require('../services/settings/settingsServices');
//const userId = 'D773F817-C24C-4113-B3AE-0A2FFC0E37CC';
const redisCluster =  require('../connections/redisConnection');
const events = require('events');
const eventEmitter = new events.EventEmitter();
const logger = require('../helpers/logger');
const LP = require("../helpers/loggerProfiler");

exports.getSettings = async (req, res, next) => {
  try {
    let userId = req.headers.userid;

    const redisUser= LP.startProfile();

    let result = await redisCluster.get(`settinglist_user_${userId}`);
   
    LP.doneProfile(redisUser,"Getting all User from Redis",'warn');
    if(result){
      result =JSON.parse(result);
    }
    if(!result || result.length == 0){
      let dbRecord = await settingsServices.getSettingsData(userId);
       result= dbRecord.Items;
       setTimeout(()=>{
        eventEmitter.emit('setSettings',{res:dbRecord.Items,userId});
       },100)
    }
    const data = result.map(({SettingKey,SettingValue}) => ({ key: SettingKey, value: SettingValue }));
    res.status(200).send(data);
  }catch(err){
    logger.error(JSON.stringify(err))
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}

exports.addSettings = async (req, res, next) => {
  try {
    const result = await settingsServices.addSettingsData(req,res);
    setTimeout(()=>{
      result.req =req;
      result.res = res;
      eventEmitter.emit('addSettings',result);
    },100)

   // let apiResponse = response.generate(1,'Setting was added successfully');
    return res.status(200).send({
      "code":1,
      "message":"Setting was added successfully"
    })
  }catch(err){
    logger.error(JSON.stringify(err))
    if (!err.statusCode) {
      err.message = "Item couldn't be added (DB insert failure)";
      err.statusCode = 500;
    }
    next(err);
  }
}

exports.updateSettings = async (req, res, next) => {
  try {
    const result = await settingsServices.updateSettingsData(req,res);
    setTimeout(()=>{
      eventEmitter.emit('updateSettings',result);
    },100)
   // let apiResponse = response.generate(1, 'Setting was updated successfully');
    return res.status(200).send({
      "code":1,
      "message":"Setting was updated successfully"
    })
  }catch(err){
    logger.error(JSON.stringify(err))
    if (!err.statusCode) {
      err.message = "Item couldn't be added (DB insert failure)";
      err.statusCode = 500;
    }
    next(err);
  }
}

exports.deleteSettings = async (req, res, next) => {
  try {
    const result = await settingsServices.deleteSettingsData(req,res);
    setTimeout(()=>{
      eventEmitter.emit('deleteSettings',result);
    },100)
    //let apiResponse = response.generate(1, 'Delete successful');
    res.status(200).send({
      "code":1,
      "message":"Delete successful"
    });
  }catch(err){
    if (!err.statusCode) {
      err.message = "Item couldn't be deleted (DB delete failure)";
      err.statusCode = 2530;
    }
    next(err);
  }
}





// Fire event list

// on set setting
eventEmitter.on('setSettings', function(data){
  redisCluster.set(`settinglist_user_${data.userId}`, JSON.stringify(data.res));
  return true;
});

// on add setting
eventEmitter.on('addSettings', async function(data){
  // console.log(data);
  let redisData= await redisCluster.get(`settinglist_user_${data.UserId}`);
  let newArry = JSON.parse(redisData) || [];
  if(newArry.length == 0){
    let dbRecord = await settingsServices.getSettingsData(data.req.headers.userid);
    newArry= dbRecord.Items;
  }else{
    newArry.push({Id:data.Id,SettingKey:data.SettingKey,SettingValue:data.SettingValue})
  }
  redisCluster.set(`settinglist_user_${data.UserId}`, JSON.stringify(newArry));
  return true;
});


// on update setting apis
eventEmitter.on('updateSettings', async function(data){
  let redisData= await redisCluster.get(`settinglist_user_${data.UserId}`);
  let newArry = JSON.parse(redisData);
  let objIndex = newArry.findIndex((obj => obj.Id == data.Id))
  newArry[objIndex].SettingValue= data.SettingValue;
 
  redisCluster.set(`settinglist_user_${data.UserId}`, JSON.stringify(newArry));
  return true;
});

// on delete setting apis
eventEmitter.on('deleteSettings', async function(data){
  let redisData= await redisCluster.get(`settinglist_user_${data.UserId}`);
  let newArry = JSON.parse(redisData);
  let objIndex = newArry.findIndex((obj => obj.Id == data.Id))
  if(objIndex  != -1){
    newArry.splice(objIndex,1);
  }
  redisCluster.set(`settinglist_user_${data.UserId}`, JSON.stringify(newArry));
  return true;
});
